var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="800">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1635003111697.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1635003111697-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1635003111697-ie8.css" /><![endif]-->\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
\
    <div id="s-bb6a2cf1-cf7e-446c-838c-a667ec15a368" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="EVENTOS" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/bb6a2cf1-cf7e-446c-838c-a667ec15a368-1635003111697.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/bb6a2cf1-cf7e-446c-838c-a667ec15a368-1635003111697-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/bb6a2cf1-cf7e-446c-838c-a667ec15a368-1635003111697-ie8.css" /><![endif]-->\
      <div class="freeLayout">\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Group_1" datasizewidth="1024.0px" datasizeheight="70.0px" >\
        <div id="s-Rectangle_1" class="pie rectangle manualfit firer commentable non-processed" customid="Rectangle_1"   datasizewidth="937.5px" datasizeheight="70.0px" datasizewidthpx="937.511400447061" datasizeheightpx="70.00000000000006" dataX="171.0" dataY="40.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_2" class="pie rectangle manualfit firer mouseenter mouseleave mousedown mouseup click commentable non-processed" customid="Rectangle_2"   datasizewidth="160.0px" datasizeheight="43.0px" datasizewidthpx="160.0" datasizeheightpx="42.99999999999994" dataX="171.0" dataY="40.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_2_0">MAPA</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_3" class="pie rectangle manualfit firer mouseenter mouseleave mousedown mouseup commentable non-processed" customid="Rectangle_2"   datasizewidth="160.0px" datasizeheight="43.0px" datasizewidthpx="160.00000000000023" datasizeheightpx="42.99999999999994" dataX="406.0" dataY="40.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_3_0">EVENTOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_4" class="pie rectangle manualfit firer mouseenter mouseleave mousedown mouseup click commentable non-processed" customid="Rectangle_2"   datasizewidth="160.0px" datasizeheight="43.0px" datasizewidthpx="160.00000000000023" datasizeheightpx="42.99999999999994" dataX="642.0" dataY="40.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_4_0">COMUNIDAD</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_5" class="pie rectangle manualfit firer mouseenter mouseleave mousedown mouseup click commentable non-processed" customid="Rectangle_2"   datasizewidth="160.0px" datasizeheight="43.0px" datasizewidthpx="160.0" datasizeheightpx="43.00000000000006" dataX="877.0" dataY="40.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_5_0">CONT&Aacute;CTANOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Input" class="pie date firer commentable non-processed" customid="Input" value="" format="MM/dd/yyyy"  datasizewidth="214.0px" datasizeheight="29.0px" dataX="391.5" dataY="145.0" ><div class="backgroundLayer">\
        <div class="colorLayer"></div>\
        <div class="imageLayer"></div>\
      </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="date"  tabindex="-1"  /></div></div></div></div></div>\
      <div id="s-Input_1" class="pie date firer commentable non-processed" customid="Input" value="" format="MM/dd/yyyy"  datasizewidth="214.0px" datasizeheight="29.0px" dataX="723.0" dataY="145.0" ><div class="backgroundLayer">\
        <div class="colorLayer"></div>\
        <div class="imageLayer"></div>\
      </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="date"  tabindex="-1"  /></div></div></div></div></div>\
      <div id="s-Paragraph" class="pie richtext autofit firer ie-background commentable non-processed" customid="Desde"   datasizewidth="45.8px" datasizeheight="21.0px" dataX="327.5" dataY="149.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_0">Desde</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_1" class="pie richtext autofit firer ie-background commentable non-processed" customid="Hasta"   datasizewidth="41.3px" datasizeheight="21.0px" dataX="640.0" dataY="149.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">Hasta</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Category" class="nativedropdown firer ie-background commentable non-processed" customid="Category"    datasizewidth="144.0px" datasizeheight="20.0px" dataX="362.7" dataY="209.0"  tabindex="-1"><div class="content"><div class="valign"><div class="value">Presencial</div></div></div><div class="icon"></div><select id="s-Category-options" class="s-bb6a2cf1-cf7e-446c-838c-a667ec15a368 dropdown-options" ><option selected="selected" class="option">Presencial</option>\
      <option  class="option">Online</option></select></div>\
      <div id="s-Category_1" class="nativedropdown firer ie-background commentable non-processed" customid="Category"    datasizewidth="144.0px" datasizeheight="20.0px" dataX="554.2" dataY="209.0"  tabindex="-1"><div class="content"><div class="valign"><div class="value">Tipo de evento</div></div></div><div class="icon"></div><select id="s-Category_1-options" class="s-bb6a2cf1-cf7e-446c-838c-a667ec15a368 dropdown-options" ><option selected="selected" class="option">Tipo de evento</option>\
      <option  class="option">Deportivo</option>\
      <option  class="option">Charla</option>\
      <option  class="option">Curso</option></select></div>\
      <div id="s-Category_2" class="nativedropdown firer ie-background commentable non-processed" customid="Category"    datasizewidth="144.0px" datasizeheight="20.0px" dataX="757.3" dataY="209.0"  tabindex="-1"><div class="content"><div class="valign"><div class="value">Coste</div></div></div><div class="icon"></div><select id="s-Category_2-options" class="s-bb6a2cf1-cf7e-446c-838c-a667ec15a368 dropdown-options" ><option selected="selected" class="option">Coste</option>\
      <option  class="option">Gratuito</option>\
      <option  class="option">&euro;</option>\
      <option  class="option">&euro;&euro;</option>\
      <option  class="option">&euro;&euro;&euro;</option></select></div>\
\
      <div id="s-Blog-3" class="group firer ie-background commentable non-processed" customid="Blog-3" datasizewidth="900.0px" datasizeheight="810.0px" >\
        <div id="s-Bg" class="pie rectangle manualfit firer commentable non-processed" customid="Bg"   datasizewidth="1004.0px" datasizeheight="469.0px" datasizewidthpx="1004.0000000000001" datasizeheightpx="468.9999999999999" dataX="111.0" dataY="317.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Bg_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_9" class="pie richtext manualfit firer ie-background commentable non-processed" customid="El pr&oacute;ximo s&aacute;bado 30/10/2"   datasizewidth="749.5px" datasizeheight="69.0px" dataX="321.0" dataY="391.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_9_0">El pr&oacute;ximo s&aacute;bado 30/10/2021 se celebrar&aacute; una carrera ben&eacute;fica por el C&aacute;ncer de mama. Partir&aacute; de la Puerta del Sol en Madrid.<br />Evento organizado por Enlazadas. Pinche </span><span id="rtr-s-Paragraph_9_1">aqu&iacute;</span><span id="rtr-s-Paragraph_9_2"> para m&aacute;s informaci&oacute;n.</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Title_2" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Carrera ben&eacute;fica contra e"   datasizewidth="568.3px" datasizeheight="64.0px" dataX="321.0" dataY="337.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Title_2_0">Carrera ben&eacute;fica contra el C&aacute;ncer de Mama</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_10" class="pie richtext manualfit firer ie-background commentable non-processed" customid="El lunes 1/11/2021 tendr&aacute;"   datasizewidth="749.5px" datasizeheight="69.0px" dataX="321.0" dataY="544.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_10_0">El lunes 1/11/2021 tendr&aacute; lugar de forma telem&aacute;tica la charla &quot;Cu&iacute;date, qui&eacute;rete&quot;, organizada por la fundaci&oacute;n &quot;Rutinas durante el C&aacute;ncer&quot; de mano de la doctora Souto Berrocal sobre los beneficios de una vida saludable. Pinche </span><span id="rtr-s-Paragraph_10_1">aqu&iacute;</span><span id="rtr-s-Paragraph_10_2"> para m&aacute;s informaci&oacute;n.</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Title_3" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Charla Online &quot;Cu&iacute;date, q"   datasizewidth="568.3px" datasizeheight="64.0px" dataX="321.0" dataY="490.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Title_3_0">Charla Online &quot;Cu&iacute;date, qui&eacute;rete&quot;</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_27" class="pie image firer ie-background commentable non-processed" customid="Image_27"   datasizewidth="56.0px" datasizeheight="57.1px" dataX="203.0" dataY="521.0"   alt="image" systemName="./images/df545f07-9464-4b6b-9353-d10b173ba822.svg" overlay="#F375C5">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="15px" version="1.1" viewBox="0 0 15 15" width="15px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>RSS Icon</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_27-Page-1" stroke="none" stroke-width="1">\
          	        <g fill="#B2B2B2" id="s-Image_27-Components" transform="translate(-777.000000, -995.000000)">\
          	            <g id="s-Image_27-Social" transform="translate(100.000000, 849.000000)">\
          	                <g id="s-Image_27-Small-Icons" transform="translate(521.000000, 145.000000)">\
          	                    <g id="s-Image_27-RSS-Icon" transform="translate(156.000000, 1.000000)">\
          	                        <path d="M11.836,14.618 C11.836,8.104 6.527,2.802 0.005,2.802 L0.005,0 C8.082,0 14.652,6.56 14.652,14.618 L11.836,14.618 L11.836,14.618 Z M9.672,14.618 L6.852,14.618 C6.852,12.785 6.138,11.064 4.845,9.774 C3.551,8.481 1.831,7.769 0.001,7.769 L0.001,4.966 C5.333,4.966 9.672,9.293 9.672,14.618 L9.672,14.618 Z M1.95,10.719 C3.03,10.719 3.902,11.594 3.902,12.662 C3.902,13.733 3.03,14.599 1.95,14.599 C0.874,14.599 0,13.733 0,12.662 C0,11.594 0.874,10.719 1.95,10.719 L1.95,10.719 Z" style="fill:#F375C5 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_14" class="pie image firer ie-background commentable non-processed" customid="Image_14"   datasizewidth="77.0px" datasizeheight="64.4px" dataX="193.0" dataY="368.0"   alt="image" systemName="./images/27a9b55a-9c69-4ed0-b7ee-a63d27a28d45.svg" overlay="#F375C5">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="28px" version="1.1" viewBox="0 0 33 28" width="33px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Fill 1</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_14-Page-1" stroke="none" stroke-width="1">\
          	        <g fill="#B2B2B2" id="s-Image_14-Components" transform="translate(-755.000000, -1464.000000)">\
          	            <path d="M785.606127,1477.96452 L771.488123,1492 L757.40172,1477.94086 C755.853164,1476.39538 755,1474.34525 755,1472.16108 C755,1469.97691 755.845342,1467.92678 757.39374,1466.38919 C758.934317,1464.85159 760.98842,1464 763.169008,1464 C765.357417,1464 767.41942,1464.85159 768.967818,1466.39707 L770.753386,1468.1791 C771.172107,1468.59701 771.84364,1468.59701 772.262361,1468.1791 L774.03205,1466.41284 C775.580527,1464.86736 777.642451,1464.01577 779.823039,1464.01577 C782.003468,1464.01577 784.057571,1464.86736 785.606127,1466.40496 C787.154605,1467.95044 787.999947,1470.00056 787.999947,1472.18474 C788.007768,1474.36891 787.154605,1476.41904 785.606127,1477.96452" id="s-Image_14-Fill-1" style="fill:#F375C5 !important;" />\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;